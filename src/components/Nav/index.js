import React from "react";
import "./styles.css";
import { Link } from "react-router-dom";

function Nav() {
	return (
		<div id="main-header">
			<div className="logo">
				<img alt="logo" src={require("../../assets/logo.png")} />
				<p> cine react</p>
			</div>
			<Link className="link" to="/">
				Movies
			</Link>
			<Link className="link" to="/todo">
				To Do List
			</Link>
		</div>
	);
}

export default Nav;
