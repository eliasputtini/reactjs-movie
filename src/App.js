import React from "react";
import { Provider } from "react-redux";

import "./styles.css";

import Route from "./routes";
import store from "./store";

const App = () => (
	<Provider store={store}>
		<Route />
	</Provider>
);

export default App;
