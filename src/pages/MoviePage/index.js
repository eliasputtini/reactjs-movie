import React, { useState, useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";

import { POPULAR, SEARCH_URL, IMAGE_URL } from "../../services/movieApi";

import { MdSearch, MdClose, MdStar, MdStarBorder } from "react-icons/md";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "./styles.css";

export default function MoviePage() {
	const {
		movies: { favArray },
	} = useSelector((state) => ({
		...state,
	}));

	const [search, setSearch] = useState("");

	const [timeout, setTime] = useState(null);

	const [selected, setSelected] = useState(null);

	const [movies, setMovies] = useState([]);

	const [shows, setShows] = useState([]);

	const [favorites, setFavorites] = useState([]);

	const dispatch = useDispatch();

	const changeInput = (event) => {
		var x = event.target.value;
		if (timeout) {
			clearTimeout(timeout);
		}

		setTime(
			setTimeout(() => {
				setSearch(x);
			}, 1000)
		);
	};

	useEffect(() => {
		if (favorites.length > 0) {
			dispatch({
				type: "ADD_MOVIE",
				payload: favorites,
			});
		}
	}, [favorites]);

	useEffect(() => {
		let load = localStorage.getItem("@fav");

		setFavorites(JSON.parse(load));
		console.log(favorites);
	}, []);

	useEffect(() => {
		if (search === "") {
			let newUrl = POPULAR;
			fetch(newUrl)
				.then((res) => res.json())
				.then((data) => {
					console.log("Data: ", data);
					setMovies([movies, ...data.results]);
				})
				.catch((error) => {
					console.log("Erro: ", error);
				});
		} else {
			let newUrl = SEARCH_URL + "&query=" + search;
			fetch(newUrl)
				.then((res) => res.json())
				.then((data) => {
					console.log("Data: ", data);
					setMovies([movies, ...data.results]);
				})
				.catch((error) => {
					console.log("Erro: ", error);
				});
		}
	}, [search]);

	function movieClicked(movie) {
		setSelected(movie);
	}

	function isFavorite(movie) {
		var result = favorites.findIndex((fav) => fav.title === movie.title);
		return result;
	}

	function Icon(movie) {
		var result = isFavorite(movie);
		if (result !== -1) {
			return <MdStar color="white" size="20px" />;
		} else {
			return <MdStarBorder color="white" size="20px" />;
		}
	}

	function toggleFavorites(movie) {
		var result = isFavorite(movie);

		if (result !== -1) {
			//remove
			let newArray = JSON.parse(JSON.stringify(favorites));
			newArray.splice(result, 1);
			setFavorites(newArray);
		} else {
			//add
			setFavorites([...favorites, movie]);
		}
	}

	var settings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 2,
	};

	return (
		<div className="movies">
			<div className="inputDiv">
				<input
					className="inputSearch"
					onChange={changeInput}
					placeholder="Busque por filmes, séries, programas"
				/>
				<MdSearch className="inputIcon" color="#757575" size="20px" />
			</div>
			{!selected && (
				<div className="container">
					<Slider {...settings}>
						{movies.map((movie) => {
							if (movies !== null) {
								if (movie.poster_path) {
									return (
										<div key={movie.poster_path}>
											<img
												alt="movie"
												onClick={() => movieClicked(movie)}
												src={IMAGE_URL + movie.poster_path}
											/>
										</div>
									);
								}
							}
						})}
					</Slider>
				</div>
			)}

			{selected && (
				<div className="selectedMovie">
					<img alt="movie" src={IMAGE_URL + selected.poster_path} />

					<div className="info">
						<div className="text">
							<h1>{selected.title}</h1>
							<p>{selected.overview}</p>
						</div>
						<button onClick={() => toggleFavorites(selected)}>
							{Icon(selected)}
							<p>Favoritos</p>
						</button>
					</div>
					<button className="closeButton" onClick={() => setSelected(null)}>
						<MdClose color="white" size="20px" />
					</button>
				</div>
			)}
			<h1 className="titleStyle">Favoritos</h1>
			<div className="container">
				<Slider {...settings}>
					{favorites &&
						favorites.map((movie) => {
							return (
								<div key={movie.poster_path}>
									<img
										alt="movie"
										onClick={() => movieClicked(movie)}
										src={IMAGE_URL + movie.poster_path}
									/>
								</div>
							);
						})}
				</Slider>
			</div>
		</div>
	);
}
