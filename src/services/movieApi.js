const API_KEY = "1ccce6626381d3dcb0116700924ffbc2";

const SEARCH_URL = `https://api.themoviedb.org/3/search/multi?api_key=${API_KEY}&language=en-US&page=1`;

const POPULAR = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;

const IMAGE_URL = "https://image.tmdb.org/t/p/w500/";

export { API_KEY, SEARCH_URL, IMAGE_URL, POPULAR };
