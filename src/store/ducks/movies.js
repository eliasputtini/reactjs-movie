import { createActions, createReducer } from "reduxsauce";

/**
 * Action types & creators
 */
export const { Types, Creators } = createActions({
	addMovie: ["text"],
});

/**
 * Handlers
 */
const INITIAL_STATE = {
	favArray: null,
};

const ADD_FAV = (state = INITIAL_STATE, action) => {
	localStorage.setItem("@fav", JSON.stringify(action.payload));
	console.log(action.payload + "salvo");
	return { favArray: action.payload };
};

// const LOAD_FAV = (state = INITIAL_STATE, action) => {
// 	let load = localStorage.getItem("@fav");
// 	console.log(load);
// 	return { favArray: load };
// };

/**
 * Reducer
 */
export default createReducer(INITIAL_STATE, {
	[Types.ADD_MOVIE]: ADD_FAV,
	// [Types.LOAD_ASYNC]: LOAD_FAV,
});
